﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RasPiConsole
{
    [JsonObject(MemberSerialization.OptIn)]
    public class CAppConfig
    {
        public static CLogger ms_iLogger;

        private static Lazy<CAppConfig> ms_lazyInstance = new Lazy<CAppConfig>(()=>
        {
            CAppConfig oAppConfig = null;
            string strConfigPath = "AppConfig.json";
            if (File.Exists(strConfigPath))
                try
                {
                    oAppConfig = JsonConvert.DeserializeObject<CAppConfig>(File.ReadAllText(strConfigPath));
                }
                catch (Exception oException)
                {
                    ms_iLogger.LogException(ELogLevel.Error, "Exception while deserializing the config file", oException);
                }
            else
            {
                oAppConfig = new CAppConfig();
                File.WriteAllText(strConfigPath, JsonConvert.SerializeObject(S));
            }

            return oAppConfig;
        });

        public static CAppConfig S = ms_lazyInstance.Value;

        [JsonProperty]
        public bool IsConductor { get; protected set; } = true;

        [JsonProperty]
        public CSlaveOrnamentConfig[] SlaveOrnaments { get; set; } = new[]
            {new CSlaveOrnamentConfig("172.26.2.141"), new CSlaveOrnamentConfig("172.26.2.142"),};

        [JsonProperty]
        public int DelayBeforeShowStart { get; protected set; } = 4000;

        //static CAppConfig()
        //{
        //    string strConfigPath = "AppConfig.json";
        //    if(File.Exists(strConfigPath))
        //        try
        //        {
        //            S = JsonConvert.DeserializeObject<CAppConfig>(File.ReadAllText(strConfigPath));
        //        }
        //        catch (Exception oException)
        //        {
        //            ms_iLogger.LogException(ELogLevel.Error, "Exception while deserializing the config file", oException);
        //        }
        //    else
        //    {
        //        File.WriteAllText(strConfigPath, JsonConvert.SerializeObject(S));
        //    }
        //}

        public CAppConfig()
        {
            IsConductor = true;
            SlaveOrnaments = new[]
                {new CSlaveOrnamentConfig("172.26.2.141"), new CSlaveOrnamentConfig("172.26.2.142"),};
            DelayBeforeShowStart = 4000;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }


        [JsonObject(MemberSerialization.OptIn)]
        public class CSlaveOrnamentConfig
        {

            public CSlaveOrnamentConfig()
            {
            }

            public CSlaveOrnamentConfig(string strHostName, int nSslPort = 22, string strLogin = "pi", string strPassword = "iHoliday")
            {
                HostName = strHostName;
                SslPort = nSslPort;
                Login = strLogin;
                Password = strPassword;
            }

            [JsonProperty]
            public string HostName { get; protected set; }
            [JsonProperty]
            public int SslPort { get; protected set; }
            [JsonProperty]
            public string Login { get; protected set; }
            [JsonProperty]
            public string Password { get; protected set; }
        }
    }
}
