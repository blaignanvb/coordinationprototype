﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BlazorApp1
{
    public class CAppConfig
    {

        public static CAppConfig S = new CAppConfig();

        public bool IsConductor { get; protected set; } = true;

        public CSlaveOrnamentConfig[] SlaveOrnaments { get; set; } = new[]
            {new CSlaveOrnamentConfig("172.26.2.141"), new CSlaveOrnamentConfig("172.26.2.142"),};

        public int DelayBeforeShowStart { get; protected set; } = 4000;

        public CAppConfig()
        {
            IsConductor = true;
            SlaveOrnaments = new[]
                {new CSlaveOrnamentConfig("172.26.2.141"), new CSlaveOrnamentConfig("172.26.2.142"),};
            DelayBeforeShowStart = 4000;
        }

        public class CSlaveOrnamentConfig
        {

            public CSlaveOrnamentConfig()
            {
            }

            public CSlaveOrnamentConfig(string strHostName, int nSslPort = 22, string strLogin = "pi", string strPassword = "iHoliday", string strRemotePlayCmd = "/home/pi/playattimeo/PlayAtTime")
            {
                HostName = strHostName;
                SslPort = nSslPort;
                Login = strLogin;
                Password = strPassword;
                RemotePlayCmd = strRemotePlayCmd;
            }

            public string HostName { get; protected set; }

            public int SslPort { get; protected set; } = 22;
            public string Login { get; protected set; } = "pi";
            public string Password { get; protected set; } = "iHoliday";
            public string RemotePlayCmd { get; protected set; } = "/home/pi/playattime/PlayAtTime";
        }
    }
}
