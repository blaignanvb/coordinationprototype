﻿using System;

namespace RasPiConsole
{
    public enum ELogLevel
    {
        Info,
        Error
    }

    public class CLogger
    {

        public void Log(ELogLevel eLogLevel, string strLogMesssage)
        {
            Console.WriteLine($"{strLogMesssage}");
        }

        public void LogException(ELogLevel eLogLevel, string strLogMesssage, Exception oException)
        {
            Console.WriteLine($"{strLogMesssage}\n{oException.Message}");
        }
    }
}