﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RasPiConsole
{
    class Program
    {

        private static CLogger ms_iLogger;

        static async Task Main(string[] args)
        {
            ms_iLogger = new CLogger();
            ms_iLogger.Log(ELogLevel.Info, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            ms_iLogger.Log(ELogLevel.Info, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Application Start PlayAtTime 0.5 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            ms_iLogger.Log(ELogLevel.Info, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

            ms_iLogger.Log(ELogLevel.Info, $"Called with args=\n{string.Join("\n        ", args)}");

            //Console.WriteLine($"args=\n{string.Join("\n        ", args)}");
            if (args.Length != 1 && args.Length != 2)
            {
                string strMessage = "Usage: RasPiConsole <VideoPath> [<StartTime>]";
                ms_iLogger.Log(ELogLevel.Error, strMessage);
                Console.WriteLine(strMessage);
                return;
            }
            string strVideoPath = args[0];
            if (args.Length == 2)
            {
                DateTime datetimeStart = DateTime.Parse(args[1]);
                await PlayVideoAtTime(strVideoPath, datetimeStart);
            }
            else
            {
                PlayVideo(strVideoPath);
            }
        }

        private static async Task PlayVideoAtTime(string strVideoPath, DateTime datetimeStart)
        {
            string strMessage = $"Play {strVideoPath} at {datetimeStart:HH:mm:ss:ffff}";
            ms_iLogger.Log(ELogLevel.Info, strMessage);
            Console.WriteLine(strMessage);
            bool bErrorWaiting = true;
            TimeSpan timespanWait = datetimeStart - DateTime.Now;
            if (timespanWait > TimeSpan.Zero)
            {
                await Task.Delay(timespanWait);
                ms_iLogger.Log(ELogLevel.Info, $"Calling PlayVideo {strVideoPath}");
                PlayVideo(strVideoPath);
            }
            else
            {
                PlayVideo(strVideoPath);
                strMessage = $"Error waiting for video to start time has past by {timespanWait.TotalMilliseconds} ms";
                ms_iLogger.Log(ELogLevel.Info, strMessage);
                Console.WriteLine(strMessage);
            }
        }

        private static async Task PlayVideo(string strVideoPath)
        {
            string strStatus = "Playing";
            string strStatus2 = $"Playing {strVideoPath} at {DateTime.Now:HH:mm:ss:ffff}";
            ms_iLogger.Log(ELogLevel.Info, strStatus2);
            Console.WriteLine(strStatus2);
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "omxplayer";
            startInfo.Arguments = strVideoPath;
            process.EnableRaisingEvents = true;
            process.StartInfo = startInfo;
            process.Exited += (sender, args) =>
            {
                strStatus = "Idle";
                ms_iLogger.Log(ELogLevel.Info, "Video play completed");
                Console.WriteLine($"Video play completed");
            };
            process.Start();
        }
    }
}
