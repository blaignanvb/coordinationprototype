﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using Renci.SshNet.Async;
using Renci.SshNet.Common;
using Renci.SshNet.Sftp;

namespace TestCoreCons
{
    public class CSshClient
    {
        private string m_strHost;
        private int m_nPort;
        private string m_strUserName;
        private string m_strPassword;
        private TimeSpan m_timespanCmdTimeOut = new TimeSpan(0, 0, 0, 750);

        private string m_strLastCmdError;
        private int m_nLastCmdExitCode;


        private SshClient m_oSshClient;
        private SshCommand m_oSshCommand;


        //protected SshCommand SshCommand => m_oSshCommand??m_oSshClient?.CreateCommand();

        public CSshClient(string strHost, int nPort, string strUserName, string strPassword)
        {
            m_strHost = strHost;
            m_nPort = nPort;
            m_strUserName = strUserName;
            m_strPassword = strPassword;
        }

        public void ConnectSsh()
        {
            if (m_oSshClient == null)
                m_oSshClient = new SshClient(m_strHost, m_nPort, m_strUserName, m_strPassword);
            if (m_oSshClient.IsConnected)
                return;
            m_oSshClient.Connect();
            m_oSshCommand = null;
        }

        public async Task<string>  SendCommand(string strCommand, int? nTimeOut_ms = null)
        {
            if (nTimeOut_ms != null)
                m_timespanCmdTimeOut = new TimeSpan(0, 0, 0, (int) nTimeOut_ms);
            //string strReply;
            ConnectSsh();
            string strReply;
            if(m_oSshCommand == null)
            {
                m_oSshCommand = m_oSshClient.CreateCommand(strCommand);
                m_oSshCommand.CommandTimeout = m_timespanCmdTimeOut;
                strReply = await m_oSshCommand.ExecuteAsync();
            }
            else
            {
                strReply = await m_oSshCommand.ExecuteAsync(strCommand);
                m_oSshCommand.CommandTimeout = m_timespanCmdTimeOut;
            }
            if(m_oSshCommand.ExitStatus!=0)
                m_strLastCmdError = m_oSshCommand.Error;
            m_nLastCmdExitCode = m_oSshCommand.ExitStatus;
            return strReply;
        }
    }

}
